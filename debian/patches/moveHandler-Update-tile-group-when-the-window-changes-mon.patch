From: Leleat <atule@pm.me>
Date: Sat, 29 Jul 2023 20:13:46 +0200
Subject: moveHandler: Update tile group when the window changes monitor

Fixes https://github.com/Leleat/Tiling-Assistant/issues/275, https://github.com/Leleat/Tiling-Assistant/issues/278

(cherry picked from commit 0095abae600fe3730f8056f2419b7c068fad9ea2)
---
 .../src/extension/moveHandler.js                   | 60 +++++++++++-----------
 1 file changed, 29 insertions(+), 31 deletions(-)

diff --git a/tiling-assistant@leleat-on-github/src/extension/moveHandler.js b/tiling-assistant@leleat-on-github/src/extension/moveHandler.js
index 50eaac0..7c2211c 100644
--- a/tiling-assistant@leleat-on-github/src/extension/moveHandler.js
+++ b/tiling-assistant@leleat-on-github/src/extension/moveHandler.js
@@ -168,14 +168,7 @@ var Handler = class TilingMoveHandler {
             this._pointerDidntMove = false;
             this._movingTimerDuration = 20;
             this._movingTimeoutsSinceUpdate = 0;
-
-            const activeWs = global.workspace_manager.get_active_workspace();
-            const monitor = global.display.get_current_monitor();
-            const workArea = new Rect(activeWs.get_work_area_for_monitor(monitor));
-
-            const topTileGroup = Twm.getTopTileGroup({ skipTopWindow: true });
-            const tRects = topTileGroup.map(w => w.tiledRect);
-            const freeScreenRects = workArea.minus(tRects);
+            this._topTileGroup = Twm.getTopTileGroup({ skipTopWindow: true });
 
             // When low performance mode is enabled we use a timer to periodically
             // update the tile previews so that we don't update the tile preview
@@ -188,8 +181,6 @@ var Handler = class TilingMoveHandler {
                         this,
                         grabOp,
                         window,
-                        topTileGroup,
-                        freeScreenRects,
                         true
                     )
                 );
@@ -199,7 +190,7 @@ var Handler = class TilingMoveHandler {
                     // 'Quick throws' of windows won't create a tile preview since
                     // the timeout for onMoving may not have happened yet. So force
                     // 1 call of the tile preview updates for those quick actions.
-                    this._onMoving(grabOp, window, topTileGroup, freeScreenRects);
+                    this._onMoving(grabOp, window);
                     this._onMoveFinished(window);
                 });
 
@@ -211,8 +202,6 @@ var Handler = class TilingMoveHandler {
                         this,
                         grabOp,
                         window,
-                        topTileGroup,
-                        freeScreenRects,
                         false
                     )
                 );
@@ -276,7 +265,9 @@ var Handler = class TilingMoveHandler {
         this._favoriteLayout = [];
         this._favoritePreviews?.forEach(p => p.destroy());
         this._favoritePreviews = [];
+        this._freeScreenRects = [];
         this._anchorRect = null;
+        this._topTileGroup = null;
         this._tilePreview.close();
         this._currPreviewMode = MoveModes.ADAPTIVE_TILING;
         this._isGrabOp = false;
@@ -291,7 +282,7 @@ var Handler = class TilingMoveHandler {
     // the current position.
     // Without the lowPerfMode enabled this will be called whenever the window is
     // moved (by listening to the position-changed signal)
-    _onMoving(grabOp, window, topTileGroup, freeScreenRects, lowPerfMode = false) {
+    _onMoving(grabOp, window, lowPerfMode = false) {
         const [x, y] = global.get_pointer();
         const currPointerPos = { x, y };
 
@@ -378,7 +369,7 @@ var Handler = class TilingMoveHandler {
                 this._edgeTilingPreview(window, grabOp);
                 break;
             case MoveModes.ADAPTIVE_TILING:
-                this._adaptiveTilingPreview(window, grabOp, topTileGroup, freeScreenRects);
+                this._adaptiveTilingPreview(window, grabOp);
                 break;
             case MoveModes.FAVORITE_LAYOUT:
                 this._favoriteLayoutTilingPreview(window);
@@ -392,13 +383,22 @@ var Handler = class TilingMoveHandler {
     _preparePreviewModeChange(newMode, window) {
         this._tileRect = null;
         this._ignoreTA = false;
+        this._topTileGroup = Twm.getTopTileGroup({ skipTopWindow: true });
+
+        const activeWs = global.workspace_manager.get_active_workspace();
+        const monitor = global.display.get_current_monitor();
+        const workArea = new Rect(activeWs.get_work_area_for_monitor(monitor));
+        const tRects = this._topTileGroup.map(w => w.tiledRect);
+        this._freeScreenRects = workArea.minus(tRects);
 
         switch (this._currPreviewMode) {
             case MoveModes.ADAPTIVE_TILING:
+                this._monitorNr = global.display.get_current_monitor();
                 this._splitRects.clear();
                 this._anchorRect = null;
                 break;
             case MoveModes.FAVORITE_LAYOUT:
+                this._monitorNr = global.display.get_current_monitor();
                 this._favoritePreviews.forEach(p => {
                     p.ease({
                         opacity: 0,
@@ -606,16 +606,16 @@ var Handler = class TilingMoveHandler {
      *
      * @param {Meta.Window} window
      * @param {Meta.GrabOp} grabOp
-     * @param {Meta.Window[]} topTileGroup
-     * @param {Rect[]} freeScreenRects
      */
-    _adaptiveTilingPreview(window, grabOp, topTileGroup, freeScreenRects) {
-        if (!topTileGroup.length) {
+    _adaptiveTilingPreview(window, grabOp) {
+        if (!this._topTileGroup.length) {
             this._edgeTilingPreview(window, grabOp);
             return;
         }
 
-        const screenRects = topTileGroup.map(w => w.tiledRect).concat(freeScreenRects);
+        const screenRects = this._topTileGroup
+            .map(w => w.tiledRect)
+            .concat(this._freeScreenRects);
         const hoveredRect = screenRects.find(r => r.containsPoint(this._lastPointerPos));
         if (!hoveredRect) {
             this._tilePreview.close();
@@ -645,9 +645,9 @@ var Handler = class TilingMoveHandler {
             const atRightEdge = this._lastPointerPos.x > hoveredRect.x2 - edgeRadius;
 
             atTopEdge || atBottomEdge || atLeftEdge || atRightEdge
-                ? this._adaptiveTilingPreviewGroup(window, hoveredRect, topTileGroup,
+                ? this._adaptiveTilingPreviewGroup(window, hoveredRect,
                     { atTopEdge, atBottomEdge, atLeftEdge, atRightEdge })
-                : this._adaptiveTilingPreviewSingle(window, hoveredRect, topTileGroup);
+                : this._adaptiveTilingPreviewSingle(window, hoveredRect);
         }
     }
 
@@ -662,9 +662,8 @@ var Handler = class TilingMoveHandler {
      *
      * @param {Meta.Window} window
      * @param {Rect} hoveredRect
-     * @param {Meta.Window[]} topTileGroup
      */
-    _adaptiveTilingPreviewSingle(window, hoveredRect, topTileGroup) {
+    _adaptiveTilingPreviewSingle(window, hoveredRect) {
         const atTop = this._lastPointerPos.y < hoveredRect.y + hoveredRect.height * .25;
         const atBottom = this._lastPointerPos.y > hoveredRect.y + hoveredRect.height * .75;
         const atRight = this._lastPointerPos.x > hoveredRect.x + hoveredRect.width * .75;
@@ -688,7 +687,7 @@ var Handler = class TilingMoveHandler {
         this._tilePreview.open(window, this._tileRect.meta, monitor);
         this._splitRects.clear();
 
-        const hoveredWindow = topTileGroup.find(w => {
+        const hoveredWindow = this._topTileGroup.find(w => {
             return w.tiledRect.containsPoint(this._lastPointerPos);
         });
 
@@ -713,15 +712,14 @@ var Handler = class TilingMoveHandler {
      *
      * @param {Meta.Window} window
      * @param {Rect} hoveredRect
-     * @param {Meta.Window[]} topTileGroup
      * @param {object} hovered contains booleans at which position the
      *      `hoveredRect` is hovered.
      */
-    _adaptiveTilingPreviewGroup(window, hoveredRect, topTileGroup, hovered) {
+    _adaptiveTilingPreviewGroup(window, hoveredRect, hovered) {
         // Find the smallest window that will be affected and use it to calculate
         // the sizes of the preview. Determine the new tileRects for the rest
         // of the tileGroup via Rect.minus().
-        const smallestWindow = topTileGroup.reduce((smallest, w) => {
+        const smallestWindow = this._topTileGroup.reduce((smallest, w) => {
             if (hovered.atTopEdge) {
                 if (w.tiledRect.y === hoveredRect.y || w.tiledRect.y2 === hoveredRect.y)
                     return w.tiledRect.height < smallest.tiledRect.height ? w : smallest;
@@ -756,7 +754,7 @@ var Handler = class TilingMoveHandler {
         // of the smallestWindow.
         if (hovered.atTopEdge || hovered.atBottomEdge) {
             const getX1X2 = alignsAt => {
-                return topTileGroup.reduce((x1x2, w) => {
+                return this._topTileGroup.reduce((x1x2, w) => {
                     const currX = x1x2[0];
                     const currX2 = x1x2[1];
                     return alignsAt(w)
@@ -790,7 +788,7 @@ var Handler = class TilingMoveHandler {
         // the smallestWindow.
         } else {
             const getY1Y2 = alignsAt => {
-                return topTileGroup.reduce((y1y2, w) => {
+                return this._topTileGroup.reduce((y1y2, w) => {
                     const currY = y1y2[0];
                     const currY2 = y1y2[1];
                     return alignsAt(w)
@@ -827,7 +825,7 @@ var Handler = class TilingMoveHandler {
         this._tilePreview.open(window, this._tileRect.meta, monitor);
         this._splitRects.clear();
 
-        topTileGroup.forEach(w => {
+        this._topTileGroup.forEach(w => {
             const leftOver = w.tiledRect.minus(this._tileRect);
             const splitRect = leftOver[0];
             // w isn't an affected window.
